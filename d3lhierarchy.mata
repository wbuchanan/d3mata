mata: 

class d3lhierarchy { 
	private:
	string				scalar	hierarchy
	
	public:
	void						new(), destroy(), init(), hierarchy(), 
								children(), links(), revalue(), sort(), value()
	string				scalar	get(), complete()
}

string scalar d3lhierarchy::get() { 
    return(this.hierarchy)
}


string scalar d3lhierarchy::complete() { 
    string scalar hierarchyObject 
    hierarchyObject = this.get() + ";"
    return(hierarchyObject)
}


void d3lhierarchy::init(string scalar vnm, | string scalar arguments) {
	if (arguments != "") {
		this.hierarchy = "var " + vnm + " = " + arguments
	}
	else {
		this.hierarchy = vnm
	}	
}


void d3lhierarchy::new() {
}

void d3lhierarchy::destroy() {
}

void d3lhierarchy::hierarchy(string scalar root) { 
    this.hierarchy = this.get() + ".hierarchy(" + root + ")"
}


void d3lhierarchy::children(| string scalar accessor) {
	if (accessor != "") {
		this.hierarchy = this.get() + ".children(" + accessor + ")"
	}
	else {
		this.hierarchy = this.get() + ".children()"
	}
}


void d3lhierarchy::links(string scalar nodes) { 
    this.hierarchy = this.get() + ".links(" + nodes + ")"
}


void d3lhierarchy::revalue(string scalar root) { 
    this.hierarchy = this.get() + ".revalue(" + root + ")"
}


void d3lhierarchy::sort(| string scalar comparator) { 
	if (comparator != "") {
		this.hierarchy = this.get() + ".sort(" + comparator + ")"
	}
	else {
		this.hierarchy = this.get() + ".sort()"
	}
}	


void d3lhierarchy::value(| string scalar value) {
	if (value != "") {
		this.hierarchy = this.get() + ".value(" + value + ")"
	}
	else {
		this.hierarchy = this.get() + ".value()"
	}
}


end


